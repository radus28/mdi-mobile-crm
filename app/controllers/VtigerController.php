<?php

class VtigerController extends BaseController {
	
	protected $layout = "layouts.main";
	protected $deny_create = false;
	protected $list_detail_link_fields = array();

	public function __construct(){
		$this->afterFilter($this->_setLayoutVars());
	}

	protected function _setLayoutVars(){
		$this->title = $this->title . ' | ' . $this->page_title;
		View::share('controllerName', get_called_class());		
		View::share('modelName', $this->page);		
		View::share('pageTitle', $this->page_title);	

		if($this->deny_create){
			View::share('denyCreate', true);		
		}
	}

	public function getList($page = 1)
	{
		$mod = new $this->model();

		// set default sort order
		$sortSessId = 'sort.'.$mod::getModuleIdentifier();
		if(!empty($this->default_sort) && !Session::has($sortSessId)){
			$field = $this->default_sort[0];
			$order = $this->default_sort[1];

			if($mod->isSortable($field)){
				Session::put('sort.' . $mod::getModuleIdentifier(), array($field, $order));
			}
		}
		
		$list = $mod->getList($page);

		$table = array();
		$table['headers'] = $mod->listFields;
		$table['data'] = $list;

		// defining the view
		$this->layout->content = View::make($this->views_dir.'.list');
		$this->layout->content->table = $table;
		$this->layout->content->ctrlIns = $this;

		$this->layout->content->sort = Session::get('sort.' . $mod::getModuleIdentifier());

		// get pagination
		$count = $mod->getTotal();
		$pages = ceil($count / $mod::$perpage);
		if($pages > 1){
			$this->layout->content->pages = $pages;
			$this->layout->content->current_page = $page;
		}

		// get fields which link to the record details page
		array_walk($this->list_detail_link_fields, function(&$val)use($mod){ $val = $mod->fields[$val][0]; });
		$this->layout->content->detail_link_fields = $this->list_detail_link_fields;

	}

	public function createForm($values = array()){
		$mod = new $this->model();
		$item = (object) $values;

		$mod->item = $item;
		$this->layout->content = View::make($this->views_dir.'.edit');
		$this->layout->content->mode = 'create';
		$this->layout->content->item = $mod;
	}

	public function editForm($id = 0){
		$mod = new $this->model();
		$item = $mod->getItem($id);
		$mod->item = $item;
		
		$this->layout->content = View::make($this->views_dir.'.edit');
		$this->layout->content->mode = 'edit';
		$this->layout->content->item = $mod;
	}

	public function detailView($id = 0){
		$mod = new $this->model();
		$item = $mod->getItem($id);
		$mod->item = $item;

		$this->layout->content = View::make($this->views_dir.'.detail');
		$this->layout->content->item = $mod;
	}
	
	public function changeSort($field = '', $order = 'ASC'){
		$model = $this->model;

		$item = new $model();
		
		if($item->isSortable($field)){
			Session::put('sort.' . $model::getModuleIdentifier(), array($field, $order));
		}

		// TODO: Change below request segment to get proper controller name
		return Redirect::to(Request::segment(1) . '/list');
	}

	public function changeFilter($field = ''){
		$value = Input::get($field);
		$model = $this->model;

		if(empty($value)){
			Session::forget('filter.' . $model::getModuleIdentifier() . '.' . $field);
		}else{
			$item = new $model();
			Session::put('filter.' . $model::getModuleIdentifier() . '.' . $field, $value);
		}

		return Redirect::to(Request::segment(1) . '/list');

	}	

	public function getAdditionalListFieldData($currField, $rowDetails){
		return null;
	}
}

?>