<?php

class UsersController extends BaseController {

	protected $layout = "layouts.main";

	public function __construct() {
		// $this->beforeFilter('csrf', array('on'=>'post'));
//		$this->beforeFilter('auth', array('only'=>array('getDashboard')));
		$this->beforeFilter('logged', array('only'=>array('getLogin', 'postSigninldap')));
	}

	public function getLogin() {
//             echo 'Login Action';
		$this->layout->content = View::make('users.login');
	}

	public function postSignin() {

		$user = false;
		$username = Input::get('username');
		$password = Input::get('password');

		// make username case insensitive
		$username = strtolower($username);
var_dump(User::checkIfUser($username, $password)); exit;		
		if($ldapuser = User::checkIfUser($username, $password)){

			$user = User::where('username', '=', $username)->first();
			if(!$user){
				$user = new User();
			}

			$user->firstname = $ldapuser->first_name;
			$user->lastname = $ldapuser->last_name;
			$user->username = $ldapuser->username;
			$user->email = $ldapuser->email;
			$user->role = $ldapuser->role;
			$user->ldap_other = $ldapuser->ldap_other;

			// get vtiger user id
			$vtuser = User::checkIfVtigerUser($username);
			$user->vtigerid = ($vtuser) ? $vtuser->id : 0;	

			$user->save();

		/*}else if($vtuser = User::checkIfVtigerUser($username)){
			
			$user = new User();
			$user->firstname = $vtuser->first_name;
			$user->lastname = $vtuser->last_name;
			$user->username = $vtuser->user_name;
			$vtuserid = explode('x', $vtuser->id);
			$user->vtigerid = $vtuserid[0];
			$user->email = $vtuser->email1;
			$user->save();*/
		}

		if ($user) {
			Auth::login($user);

			$redirect_url = (Input::has('redirect_url')) ? Input::get('redirect_url') : 'users/dashboard';
			return Redirect::to($redirect_url)->with('message', 'You are now logged in!');
		} else {
			$redirect_url = (Input::has('redirect_url')) ? '?redirect_url=' . Input::get('redirect_url') : '';
			return Redirect::to('users/login'.$redirect_url)
				->with('error', true)
				->with('message', 'Your username/password combination was incorrect')
				->withInput();
		}
	}

	public function getDashboard() {
		// $this->layout->content = View::make('users.dashboard');
		return Redirect::to('investigation/list');

	}

	public function getLogout() {
		Auth::logout();
		Session::flush();
		return Redirect::to('users/login')->with('message', 'Your are now logged out!');
	}
}