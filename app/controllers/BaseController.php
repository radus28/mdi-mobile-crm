<?php

class BaseController extends Controller {

    	protected $page = '';
	protected $title = 'CRM-MOBILEAPP';
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
			$this->layout->page = $this->page;
			$this->layout->title = $this->title;

			if(Auth::check()){
				if(!$user_displ_name = Session::get('curr_logged_user')){
					$user_displ_name = ucwords((empty(Auth::user()->firstname)) ? trim(Auth::user()->lastname) : trim(Auth::user()->firstname));
					Session::put('curr_logged_user', $user_displ_name);
				}

				$this->layout->logged_user = $user_displ_name;
			}
		}   
	}

}
