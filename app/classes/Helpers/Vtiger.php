<?php 

namespace Helpers;

class Vtiger {

	public static $username = 'admin';

	public static function runQuery($query){
		$response = self::doService('query', array('query' => $query));
		return $response;
	}

	public static function doService($operation, $params = array(), $first_attempt = true){

		$url = \Config::get('vtiger.endpointUrl');

		if(isset($params['post'])){
			$post = array_merge(array(
				'operation'=>$operation,
				'sessionName'=>\Session::get('vtiger.sessionId')
			), $params);

			$response = json_decode(\Helpers\CURL::callURL($url, $post));
		}else{
			$response = json_decode(\Helpers\CURL::callURL($url.'?operation='.$operation.'&sessionName='.\Session::get('vtiger.sessionId').'&'.http_build_query($params)));			
		}
		
		// if it's not a valid WS, it returns null
		if($response == null){
			die($operation.' failed: not a valid webservice');
		}

		if($response->success==false) {

			if(in_array($response->error->code, array('AUTHENTICATION_REQUIRED', 'INVALID_SESSIONID', 'ACCESS_DENIED')) && $first_attempt){
				$username = self::$username;
				// all werbservices are carried by only one user so below is not needed
				// if(\Auth::check()){
				// 	$username = \Auth::user()->username;
				// 	echo $username;
				// }

				self::login($username);
				$response = self::doService($operation, $params, false);
			}else{
		    	die($operation.' failed:'.@$response->error->message.' - Code: '.@$response->error->code);
			}

		}

		return $response;

	}

	public static function getChallenge($username){

		$url = \Config::get('vtiger.endpointUrl');
		$response = json_decode(\Helpers\CURL::callURL($url.'?operation=getchallenge&username='.$username));
	
		if($response->success==false) {
		    die('getchallenge failed:'.$response->error->message);
		}

		//operation was successful get the token from the reponse.
		$challengeToken = $response->result->token;
	
		return $challengeToken;
	}

	public static function getSessionId($username){

		$url = \Config::get('vtiger.endpointUrl');
		$userAccessKey = \Config::get('vtiger.accessKey');
		$challengeToken = self::getChallenge($username);

		$generatedKey = md5($challengeToken.$userAccessKey);

		$response = json_decode(\Helpers\CURL::callURL($url.'?operation=login', array(
			'username'=>$username,
			'accessKey'=>$generatedKey
			)));
	
		if($response->success==false) {
		    die('login failed:'.$response->error->message);
		}

		//login successful extract sessionId and userId from LoginResult to it can used for further calls.
		\Session::put('vtiger.sessionId', $response->result->sessionName);
		\Session::put('vtiger.userId', $response->result->userId);

		return true;
	}

	public static function login($username){
		if(!empty(self::$sessionId)){
			return true;
		}else{
			self::getSessionId($username);
		}
	}

	public static function sortIcon($sort, $field){
		$html = '<span class="icon-cont">';

        if($sort[0]==$field){
        	$class = ($sort[1]=='DESC') ? 'glyphicon-sort-by-attributes-alt' : 'glyphicon-sort-by-attributes';
            $html .= '<span class="current glyphicon '.$class.'"></span>';
        }
		
		$class = ($sort[0]==$field&&$sort[1]=='ASC') ? 'glyphicon-sort-by-attributes-alt' : 'glyphicon-sort-by-attributes';
        $html .= '<span class="alt glyphicon '.$class.'"></span>';

		return $html . '</span>';

	}

	public static function getFieldSortOrder($sort, $field){
		return ($sort[0]==$field&&$sort[1]=='ASC') ? 'DESC' : '';
	}
}

?>