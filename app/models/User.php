<?php


use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public static function checkIfVtigerUser($username){

		$query = "SELECT * FROM users WHERE user_name = '{$username}';";
		$result = Helpers\Vtiger::runQuery($query);

		return (count($result->result) > 0) ? $result->result[0] : false;
	}

	public static function getVtigerUserDetails($id){

		$query = "SELECT * FROM users WHERE id = '{$id}';";
		$result = Helpers\Vtiger::runQuery($query);

		return (count($result->result) > 0) ? $result->result[0] : false;
	}

	public static function checkIfUser($username, $password){
return 'model ok';
//    	$result = ldapAuthenticateAndReturn($username, $password);
	    if ($result == NULL) {
	    	return false;
    	} else {
			$user = self::getLDAPUserDetails($username, $result);
			return $user;
		}

	}

	public static function getLDAPUserDetails($username, $otherDetails = false){

		include_once 'ldap/getUserDetails.php';

 		$result=getUserRole($username);

 		// try again if it fails the first time
     	if($result == "0" || $result =="-1" || empty($result)){
 			$result=getUserRole($username);
		}

     	if($result == "0" || $result =="-1" || empty($result)){
     		return false;
 		}else{
			$result = json_decode($result);

			$full_name = explode(" ", trim($result->USERNM));
			$result->first_name = $full_name[0];
			unset($full_name[0]);
			$result->last_name = implode(" ", $full_name);
			$result->username = $result->USERID;
			$result->role = $result->USERTP;
			$result->email = trim($result->USEREM);
			$result->ldap_other = '';

			if($otherDetails){
				$otherDetails = $otherDetails[0];

				// user ldap details
				$ldapUserDetail = array();
				$ldapUserDetail['department'] = @$otherDetails['department'][0];
				$ldapUserDetail['epmno'] = @$otherDetails['extensionattribute3'][0];
				$ldapUserDetail['costcenter'] = @$otherDetails['extensionattribute4'][0];
				$ldapUserDetail['faxno'] = @$otherDetails['facsimiletelephonenumber'][0];
				$ldapUserDetail['telno'] = @$otherDetails['telephonenumber'][0];
				$ldapUserDetail['mail'] = @$otherDetails['mail'][0];
				$ldapUserDetail['manager'] = @$otherDetails['manager'][0];
				$ldapUserDetail['mobile'] = @$otherDetails['mobile'][0];
				$ldapUserDetail['streetaddress'] = @$otherDetails['streetaddress'][0];
				$ldapUserDetail['st'] = @$otherDetails['st'][0];
				$ldapUserDetail['postalcode'] = @$otherDetails['postalcode'][0];
				$ldapUserDetail['title'] = @$otherDetails['title'][0];
				
				$result->ldap_other = serialize($ldapUserDetail);
			}

			return $result;
     	}

	}

}