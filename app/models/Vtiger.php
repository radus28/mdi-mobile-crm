<?php

class Vtiger extends Eloquent {

	protected static $vtigerModule = ''; // for pagination
	public static $perpage = 20; // for pagination
	public static $module_desc = null; // module description array including picklist vals etc.

	function __construct(){
		$this->fields = array_merge($this->editFields, $this->fields);
	}

	public function getItem($id, $columns=array()){
		$list = Helpers\Vtiger::doService('retrieve', array('id' => $id));
		
		// determine edit permission for record
		$this->_checkEditPermission($list->result);

		return $list->result;
	}

	public function getList($page = 1){
		$fields = implode(',', array_keys($this->listFields));
		$where = static::getWhereCondition();

		// get sort order
		$order = '';
		if($sort = Session::get('sort.' . static::getModuleIdentifier())){
			$order = ' ORDER BY ' . $sort[0] . ' ' . $sort[1];
		}		

		// get limit
		if($page > 0){
			$startFrom = ($page - 1) * static::$perpage;
			$limit = 'LIMIT ' . $startFrom . ',' . static::$perpage;
		}else{
			$limit = '';
		}

		$query = "SELECT * FROM ".static::$vtigerModule." " . $where . " ".$order." ".$limit.";";

		$list = Helpers\Vtiger::runQuery($query);
		
		// determine edit permission for record
		foreach($list->result as &$item){
			$this->_checkEditPermission($item);
		}

		return $list->result;
	}

	protected function _checkEditPermission(&$item){
		return true;
	}

	public function getAll($where = ''){
		$where = (empty($where)) ? '' : "WHERE " . $where;
		
		$query = "SELECT * FROM ".static::$vtigerModule." ".$where.";";
		$list = Helpers\Vtiger::runQuery($query);

		return $list->result;
	}	

	public function getTotal(){
		
		$where = static::getWhereCondition();

		$query = "SELECT count(*) FROM ".static::$vtigerModule." " . $where . ";";		
		$list = Helpers\Vtiger::runQuery($query);

		return $list->result[0]->count;
	}

	public function saveItem(){

		$details = (array) $this->getItem($this->item->id);
		
		// extend the object with input values
		$details = array_merge($details, (array) $this->item);
		
		// validate data
		$validator = $this->validate($details);
		if($validator->fails()){
			return Redirect::back()->withErrors($validator);
		}

		$this->item = (object) $details;
		$result = Helpers\Vtiger::doService('update', array('element' => json_encode($details), 'post' => true));

		return (bool) $result->success;
	}

	public function createItem(){

		// extend the object with input values
		$details = $this->item;
		
		// validate data
		$validator = $this->validate($details);
		if($validator->fails()){
			return Redirect::back()->withErrors($validator);
		}

		$details->assigned_user_id = !empty($details->assigned_user_id) ? $details->assigned_user_id : '19x1';

		$this->item = (object) $details;
// pre($this->item);
		$result = Helpers\Vtiger::doService('create', array('element' => json_encode($details), 'elementType' => static::$vtigerModule, 'post' => true));
		$success = (bool) $result->success;

		return ($success) ? $result->result : $success;
	}	

	public function uploadImage($field = '', $filesuffix = ''){
		
		$filename = trim(Input::get($this->fields[$field][0].'_filename'));
		if(!empty($filename)){
			$filedata = trim(Input::get($this->fields[$field][0].'_content'));
			$filedata = base64_decode($filedata);
			$filedata = mb_convert_encoding($filedata, 'windows-1252');

			$savePath = Config::get('vtiger.crm_docroot').static::$uploadPath;
			$URLPath = URL::to('/').'/'.static::$uploadPath;
			$updated_filename = $this->item->crm_id . '_' . $this->fields[$field][0] . '_' . $filename;

			@unlink($savePath.$this->item->{$this->fields[$field][0]});
			file_put_contents($savePath.$updated_filename, $filedata);

			$this->item->{$this->fields[$field][0]} = $updated_filename;
			self::saveItem();
		}

		return true;
	}

	public function populate($data = array()){
		$this->item = (object) $data;
	}

	/**
	 * Generate form elements depending on field type
	 */
	public function formElem($fieldname){
		$field = $this->editFields[$fieldname];
		$fieldType = (isset($field['type'])) ? $field['type'] : 'text';
		$options = array();

		if(isset($field['ro'])){
			$options['readonly'] = true;
		}

		$value = @$this->item->$field[0];

		// if label is not set, hide lable
		if(!empty($field[1])){
			echo "<label for=\"{$field[0]}\">{$field[1]}</label>";
		}
		
		switch($fieldType){
			case 'text':
			default:
				echo Form::text($field[0], $value, array_merge(array('class'=>'form-control input-sm', 'id'=>$field[0], 'placeholder'=>$field[1]), $options));
				break;

			case 'date':
				echo Form::text($field[0], $value, array_merge(array('class'=>'form-control input-sm datepicker', 'id'=>$field[0], 'placeholder'=>'YYYY-MM-DD'), $options));
				break;
				
			// files do not get posted due to KTAS security policies, therefore, the contents of the
			// attached file is converted to a Base64 string as poasted as a normal input field
			// _content field contains this data
			// _filename contains the name of the attached file			
			case 'file':
				echo Form::hidden($field[0].'_content', '');
				echo Form::hidden($field[0].'_filename', '');
				echo Form::file($field[0], array_merge(array('class'=>'', 'id'=>$field[0], 'accept'=>'image/*', 'capture'=>'camera'), $options));
				echo "<a href=\"".Config::get('vtiger.crm_root').static::$uploadPath.$value."\" target=\"_blank\" style=\"display: block; margin-top: 5px;\"><small>{$value}</small></a>";
				break;
				
			case 'textarea':
				$options['rows'] = 3;
				echo Form::textarea($field[0], $value, array_merge(array('class'=>'form-control input-sm', 'id'=>$field[0], 'placeholder'=>$field[1]), $options));			
				break;

			case 'picklist':				
			case 'multipicklist':				
				$values = $this->getPicklistValues($fieldname);
				$value = (empty($value)) ? $values['default'] : $value;

				if($fieldType == 'multipicklist'){
					$options['multiple'] = 'true';
				}
				
				echo Form::select($field[0], $values['list'], $value, array_merge(array('class'=>'form-control input-sm', 'id'=>$field[0], 'placeholder'=>$field[1]), $options));
				break;
		}
	}

	/**
	 * Generates input fields diguised as normal textblock for details view
	 * TODO > Instead of using form input fields, use <p> tags
	 */
	public function detailElem($fieldname){
		$field = $this->editFields[$fieldname];
		$fieldType = (isset($field['type'])) ? $field['type'] : 'text';

		$options = array();
		$options['readonly'] = true;

		$value = @$this->item->$field[0];
		$value = (empty($value)) ? '' : $value;
		
		if(!empty($field[1])){
			echo "<label for=\"{$field[0]}\">{$field[1]}</label>";
		}
		
		switch($fieldType){
			case 'text':
			case 'picklist':				
			case 'date':
			default:
				echo Form::text($field[0], $value, array_merge(array('class'=>'form-control input-sm detail', 'id'=>$field[0], 'placeholder'=>$field[1]), $options));
				break;
			
			case 'file':
				echo "<a href=\"".Config::get('vtiger.crm_root').static::$uploadPath.$value."\" target=\"_blank\" style=\"display: block; margin-top: 7px;\"><small>{$value}</small></a>";
				break;

			case 'textarea':
				$options['rows'] = 3;
				echo Form::textarea($field[0], $value, array_merge(array('class'=>'form-control input-sm  detail', 'id'=>$field[0], 'placeholder'=>$field[1]), $options));			
				break;
		}
	}

	/**
	 * Get vTiger module description, including picklist values
	 */
	public static function getModuleDescription(){
		if(static::$module_desc === null){
			$data = Helpers\Vtiger::doService('describe', array('elementType' => static::$vtigerModule));
			$data = $data->result;
			$fields = array();
			foreach($data->fields as $field){
				$fields[$field->name] = $field;
			}
			$data->fields = $fields;

			static::$module_desc  = $data;
		}

		return static::$module_desc;
	}

	/**
	 * Validate inputs using Laravel Validator class. Refer the Doc
	 */
	public function validate($details){
		$checkWhat = array();
		$checkAgainst = array();

		foreach($this->validations as $k => $rules){
			$checkWhat[$this->editFields[$k][1]] = $details[$k];
			$checkAgainst[$this->editFields[$k][1]] = $rules;
		}

		return Validator::make(
		    $checkWhat,
		    $checkAgainst
		);
	}

	public static function getModuleIdentifier(){
		return strtolower(preg_replace('/\s/', '', static::$vtigerModule));
	}

	public function isSortable($field){
		return (!empty($field) && isset($this->listFields[$field]));
	}

	/**
	 * Populate picklist values
	 * TODO > Make the switch extendable by child classes 
	 * TODO > Make cURL calls singleton or cache it
	 */
	public function getPicklistValues($fieldname){
		$values = array('list'=>array(), 'default'=>"");
		$field = $this->editFields[$fieldname];

		switch($fieldname){
			case 'store_name':
				$script = Config::get('vtiger.crm_root').'KTAS/getListofStores.php';
				$data = json_decode(Helpers\CURL::callURL($script));
				$values['list'][''] = "";
				foreach ($data as $emp) {
					$opt_val = trim($emp->STORE);
				    $values['list'][$opt_val] = $opt_val;
				}

				break;

			case 'completed_by':
			case 'ca1_by_whom':
			case 'ca2_by_whom':
			case 'ca3_by_whom':
				$script = Config::get('vtiger.crm_root').'KTAS/getIncidentAssignedTo.php';
				$data = json_decode(Helpers\CURL::callURL($script));
//                                echo '<pre>';
                                
                               $values['list'][''] = "";
                               if(!is_array($data))
                                   break;
                               
				foreach ($data as $emp) {
					$opt_val = trim($emp->EAEMNM) . ':' . trim($emp->EAEMNO);
				    $values['list'][$opt_val] = $opt_val;
				}
//                                var_dump($data);exit;
				asort($values['list']);

				break;
				
			default:
				$moduleDesc  = self::getModuleDescription(); 

				if($moduleDesc->fields[$field[0]]->type->name == 'picklist' ||
					$moduleDesc->fields[$field[0]]->type->name == 'multipicklist'){
					foreach($moduleDesc->fields[$field[0]]->type->picklistValues as $value){
						$values['list'][$value->value] = $value->value;
					}
					$values['default'] = $moduleDesc->fields[$field[0]]->type->defaultValue;
				}

				break;
		}

		return $values;
	}

	public static function getFilterConditionsArr(){

		$where = array();

		if($filters = Session::get('filter.' . static::getModuleIdentifier())){
			foreach($filters as $field_name => $val){

				// if filter is NOT set to ALL
				if($val != "*"){
					$where[] = $field_name . " = '" . $val . "'";					
				}
			}
		}

		return $where;
	}

	public static function getWhereCondition($where = array()){

		$where = array_merge(static::getFilterConditionsArr(), $where);

		if(count($where) > 0){
			$where = ' WHERE ' . implode(" AND ", $where);
		}else{
			$where = '';
		}

		return $where;
	}
}

?>