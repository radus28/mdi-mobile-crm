<div class="page-header">
	<h3>Login <small>Using your VTIGER login  details</small></h3>
</div>

<div class="row">
  <div class="col-md-6 col-md-offset-3">
{{ Form::open(array('url'=>'users/signin', 'role'=>'form', 'class'=>'login')) }}

<div class="well">
<div class="form-group">
	<label for="username">Username</label>
	{{ Form::text('username', null, array('class'=>'form-control', 'id'=>'username', 'placeholder'=>'Username')) }}
</div>
<div class="form-group">
	<label for="password">Password</label>
	{{ Form::password('password', array('class'=>'form-control', 'id'=>'password', 'placeholder'=>'Password')) }}
</div>
{{ Form::submit('Login', array('class'=>'btn btn-primary'))}}
</div>

@if(Input::has('redirect_url'))
{{ Form::hidden('redirect_url', Input::get('redirect_url')) }}
@endif

{{ Form::close() }}

<script type="text/javascript">
	var oldVal = document.getElementById('username').value;
	document.getElementById('username').focus();
	document.getElementById('username').value = '';
	document.getElementById('username').value = oldVal;
</script>