<div class="navbar navbar-inverse" role="navigation"> <!-- inverse -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="{{URL::to('/')}}">Radus28 - Mobile Vtiger</a>
	</div>
	<div class="navbar-collapse collapse">
		@if(!Auth::guest())
		@if(isset($page))
		<ul class="nav navbar-nav">
			<li @if($page == 'investigation')class="active"@endif><a href="{{ URL::action('InvestigationController@getList') }}">Home</a></li>
			<li @if($page == 'compliance')class="active"@endif><a href="{{ URL::action('ComplianceController@getList') }}">Organizations</a></li>
			<li @if($page == 'roadworthy')class="active"@endif><a href="{{ URL::action('RoadworthyController@getList') }}">Contacts</a></li>
		</ul>
		@endif
		<p class="navbar-text navbar-right">Welcome, {{ $logged_user }}<span class="sep">|</span><a href="{{ URL::to('users/logout') }}" class="navbar-link"><strong>Logout</strong></a></p>
		@endif
	</div>
</div>