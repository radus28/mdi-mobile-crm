<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ $title }}</title>
	{{ HTML::style('packages/css/bootstrap.min.css') }}
	<!-- {{ HTML::style('css/bootstrap.min.css') }} -->
<!--	{{ HTML::style('css/cerulean-theme.css')}}
	{{ HTML::style('css/main.css')}}-->
	{{ HTML::script('packages/js/jquery-1.10.2.min.js') }}
	{{ HTML::script('packages/js/bootstrap.min.js') }}
	<!--{{ HTML::script('js/general.js') }}-->
	<!--[if lt IE 9]>
	{{ HTML::script('js/html5shiv.js') }}
	{{ HTML::script('js/respond.min.js') }}
	<![endif]-->
</head>
<body>
	<div class="container">
               @include('layouts.header')
		@section('message')
		@include('message')
		@show

		{{ $content }}
		<br />
		<br />
		<br />
	</div>
</body>
</html>