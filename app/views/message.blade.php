@if(Session::has('message'))
<div class="alert-wrapper">
	@if(Session::has('error'))
		<div class="alert alert-danger" style="margin: 0 auto;">{{ Session::get('message') }}</div>
	@else
		<div class="alert alert-success" style="margin: 0 auto;">{{ Session::get('message') }}</div>
	@endif
</div>
@endif

@if(count($errors)>0)
<div class="alert-wrapper">
	<div class="alert alert-danger" style="margin: 0 auto;">
            @foreach ($errors->all() as $e)
            <p>{{ $e }}</p>
            @endforeach
	</div>
</div>
@endif