@section('form-header')
<div class="page-header">
	<h3>{{ $pageTitle }} <small>@if($mode=='create')Create @elseEdit @endif  Record</small><small class="pull-right"><a href="{{ URL::action($controllerName.'@getList') }}" class="btn btn-default btn-sm">Go Back</a></small></h3>
</div>
@show

{{ Form::open(array('url'=>$modelName.'/save', 'class'=>'edit', 'role'=>'form')) }}

@section('form-fields')
@show

@if($mode=='edit')
{{ Form::hidden('id', $item->item->id) }}
@endif

<button type="submit" class="btn btn-primary">Save</button>
@section('form-additional-buttons') 
@show

{{ Form::button('Cancel', array('class'=>'btn btn-default', 'onclick' => 'javascript:window.location="'.URL::action($controllerName.'@getList').'"')) }}

{{ Form::close() }}

@section('form-js')
{{ HTML::style('packages/jquery-ui/css/flick/jquery-ui-1.10.3.custom.min.css')}}
{{ HTML::script('packages/jquery-ui/js/jquery-ui-1.10.3.custom.min.js') }}
<script type="text/javascript">
$(function(){
	$('input.datepicker:not(:disabled):not([readonly])').datepicker({
		dateFormat: 'yy-mm-dd'
	});

	// every file upload sends a binary version of data as a string as multipart is not an allowed enctype
	$("input[type='file']").each(function(){
		$(this).change(function(){
			var name = $(this).attr('name');
			var file = $(this).get(0).files[0];
			if(file){
				var reader = new FileReader();
				reader.readAsText(file, 'windows-1252');
				reader.onload = function (e){
					$("input[name='"+name+"_content']").val(tob64(e.target.result));
				}
				reader.onerror = function (e){
					alert('An error occured while attaching file. Please try again.');
				}
			}

			var filename = $(this).val().split('/').pop().split('\\').pop();
			$("input[name='"+name+"_filename']").val(filename);
		});
	});

	function tob64(str){
		return window.btoa(unescape(encodeURIComponent(str)));
	}
});
</script>
@show