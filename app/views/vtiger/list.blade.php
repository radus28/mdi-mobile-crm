<div class="page-header list">
  <div class="row">
    @section('list-header')
    <div class="col-md-{{ (isset($listHeaderColValRep)) ? $listHeaderColValRep : 12 }}">
      <h3>
        {{ $pageTitle }} 
        <small>Records List</small>
        @if(@!$denyCreate)
        <small class="pull-right"><a href="{{ URL::action($controllerName.'@createForm') }}" class="btn btn-success btn-sm">{{ (isset($createRecLabel)) ? $createRecLabel : 'Create Record' }}</a></small></h3>
        @endif
    </div>
    @show
  </div>
</div>

<div class="table-responsive">
  <table class="list-view table table-striped">
    <thead>
    	<tr>
    	   @foreach ($table['headers'] as $field => $header)
    		<th>
            <a href="{{ URL::action($controllerName.'@getList') }}/sort/{{ $field }}/{{ Helpers\Vtiger::getFieldSortOrder($sort, $field) }}">
                {{ $header }}
                {{ Helpers\Vtiger::sortIcon($sort, $field) }}
            </a>
        </th>
        @endforeach
        <th></th>
    	</tr>
    </thead>
    <tbody>
      @if(count($table['data']) == 0)
      <tr>
        <td colspan="{{ count($table['headers']) + 1 }}" class="no-records">Sorry, no matching records were found.</td>
      </tr>
      @endif
    	@foreach ($table['data'] as $row)
    	<tr>
    		@foreach ($table['headers'] as $field => $header)
    			<td>
            @if(in_array(strtolower($header), array('details')))
              <div class="ttcont"><div title="{{ $row->{$field} }}" rel="tooltip" class="tt">{{ $row->{$field} }}</div></div>
            @elseif(in_array($field, $detail_link_fields))
              <a href="{{ URL::action($controllerName.'@detailView', $row->id) }}" title="View Record Details">{{ $row->{$field} }}</a>
            @else
              {{ $row->{$field} }}
            @endif

            {{ $ctrlIns->getAdditionalListFieldData($field, $row); }}
          </td>
    		@endforeach
    		<td>
          @if(!isset($row->denyEdit))
          <a href="{{ URL::action($controllerName.'@editForm', $row->id) }}" class="btn btn-primary btn-xs" style="float: right;">Edit</a>
          @endif
        </td>
    	</tr>
    	@endforeach
    </tbody>
  </table>
</div>

@if(isset($current_page))
<ul class="pagination">
  <li class="@if($current_page==1)disabled@endif"><a href="{{ URL::action($controllerName.'@getList', ($current_page==1)?1:$current_page-1) }}">&laquo;</a></li>
  @for ($i = 1; $i <= $pages; $i++)
  <li class="@if($current_page==$i)active@endif"><a href="{{ URL::action($controllerName.'@getList', $i) }}">{{ $i }}</a></li>
  @endfor
  <li class="@if($current_page==$pages)disabled@endif"><a href="{{ URL::action($controllerName.'@getList', ($current_page==$pages)?$pages:$current_page+1) }}">&raquo;</a></li>
</ul>
@endif

<script type="text/javascript">
$(function(){
  $("[rel='tooltip']").tooltip({ placement: 'bottom' }).css('cursor', 'default');
});
</script>

@section('list-script')
@show