@section('form-header')
<div class="page-header">
	<h3>
		{{ $pageTitle }} 
		<small>Record Details</small>
		<small class="pull-right"><a href="{{ URL::action($controllerName.'@getList') }}" class="btn btn-default btn-sm">Go Back</a></small>
	</h3>
</div>
@show

@section('form-fields')
@show